from BrickPi import *
import time
import RPi.GPIO as GPIO

class Sensor:
    def getValue(self): pass

################################################################



class DistanceSensorLego(Sensor):
    def __init__(self, port,lock):
        self.port = port
        self.lock = lock
        self.lock.acquire()
        BrickPi.SensorType[self.port] = TYPE_SENSOR_ULTRASONIC_CONT
        BrickPiSetupSensors()
        self.lock.release()

    def getValue(self):
        self.lock.acquire()
        value = BrickPi.Sensor[self.port]
        self.lock.release()
        return value

################################################################

class DistanceSensorPCB(Sensor):
    def getValue(self):
        trig_duration = 0.0001
        inttimeout = 2100
        v_snd = 340.29  # speed of sound
        echo_gpio = 17
        trig_gpio = 4
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(echo_gpio, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(trig_gpio, GPIO.OUT)
        GPIO.output(trig_gpio, False)
        while True:
            # Trigger trig_gpio for trig_duration
            GPIO.output(trig_gpio, True)
            time.sleep(trig_duration)
            GPIO.output(trig_gpio, False)

            # Wait for the echo signal (or timeout)
            countdown_high = inttimeout
            while ( GPIO.input(echo_gpio) == 0 and countdown_high > 0 ):
                countdown_high -= 1

            # If we've gotten a signal
            if countdown_high > 0:
                echo_start = time.time()

                countdown_low = inttimeout
                while ( GPIO.input(echo_gpio) == 1 and countdown_low > 0 ):
                    countdown_low -= 1
                echo_end = time.time()

                echo_duration = echo_end - echo_start

            # Display the distance, unless there was a timeout on
            # the echo signal
            if countdown_high > 0 and countdown_low > 0:
                # echo_duration is in seconds, so multiply by speed
                # of sound.  Divide by 2 because of rounttrip and
                # multiply by 100 to get cm instead of m.
                distance = echo_duration * v_snd * 100 / 2
                return distance  # distance in cm
            # Wait before retriggering
        time.sleep(1)
