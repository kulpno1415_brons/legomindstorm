from BrickPi import *
################################################################

class Command():
    pass

################################################################


class Drive(Command):
    def __init__(self, portMotorLeft, portMotorRight,lock):
        self.motorLeft = portMotorLeft
        self.motorRight = portMotorRight
        self.throttle = 128
        self.lock = lock
        self.lock.acquire()
        BrickPi.MotorEnable[self.motorLeft] = 1
        BrickPi.MotorEnable[self.motorRight] = 1
        self.lock.release()

    def setThrottle(self,throttle):
        self.throttle = int(throttle*255/100) #TODO maak float

    def forward(self):
        self.move(self.throttle, self.throttle)

    def backward(self):
        self.move(-self.throttle, -self.throttle)

    def turnLeft(self):
        self.move(0,self.throttle)

    def rotateLeft(self):
        self.move(-self.throttle / 2, self.throttle / 2)

    def forwardLeft(self):
        self.move(self.throttle / 2, self.throttle)

    def turnRight(self):
        self.move(self.throttle, 0)

    def move(self,left,right):
        self.lock.acquire()
        BrickPi.MotorSpeed[self.motorLeft] = left
        BrickPi.MotorSpeed[self.motorRight] = right
        self.lock.release()

    def rotateRight(self):
        self.move(self.throttle / 2, -self.throttle / 2)

    def forwardRight(self):
        self.move(self.throttle, self.throttle / 2)

    def stop(self):
        self.move(0,0)

################################################################

class CameraRotate(Command):
    def __init__(self, MotorCamera,lock):
        self.MotorCamera = MotorCamera
        self.lock = lock
        BrickPi.MotorEnable[self.MotorCamera] = 1


    def RotateCamera(self, Direction):
        power = 0
        if (Direction == 'right'):
            sensorPort = PORT_1
            power = -125
        if (Direction == 'left'):
            sensorPort = PORT_4
            power = 125
        # BrickPi.SensorType[sensorPort] = TYPE_SENSOR_TOUCH
        # BrickPiSetupSensors()
        # time.sleep(0.5)
        # print(BrickPi.Sensor[sensorPort])
        # counter = 0
        # loopcounter = 0
        # while ((power < 256) & (power > -256) & (counter <= 1) & (loopcounter<30)):
        BrickPi.MotorSpeed[self.MotorCamera] = power
        time.sleep(3)
        #     if (Direction == 'right'):
        #         power = power + 10
        #     if (Direction == 'left'):
        #         power = power - 10
        #     time.sleep(.5)
        #     if (BrickPi.Sensor[sensorPort] == 1):
        #         counter = counter + 1
        #     if (BrickPi.Sensor[sensorPort] == 0):
        #         counter = 0
        #     loopcounter = loopcounter + 1
        # #BrickPi.MotorSpeed[self.MotorCamera] = 0
        # if (loopcounter == 30):
        #     return -1

    def RotateMotorDegrees(self,degrees):
        self.lock.acquire()
        print('rotating')
        # motorRotateDegree([30],[degrees],[self.MotorCamera])
        BrickPi.MotorSpeed[self.MotorCamera] = 30
        self.lock.release()

