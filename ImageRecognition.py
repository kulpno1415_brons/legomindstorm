import threading
import cv2
import numpy as np
import colorsys


class ImageRecognition():

    BLUR_SIZE = 5
    SKIP_STEP_SIZE = 10

    BACKGROUND_SAMPLE_DISTANCE = 50
    BACKGROUND_BGR = (87, 179, 228)
    BACKGROUND_HSV = (0.11, 0.62, 0.72)
    BACKGROUND_MASK_MUL_TOLERANCE = 0.15
    BACKGROUND_MASK_DIFF_TOLERANCE = 15


    CONTOUR_MIN_AREA = 5000
    CONTOUR_MAX_AREA = 50000

    POLYGON_FINE_EPSILON_MULTI = 0.01
    POLYGON_ROUGH_EPSILON_MULTI = 0.1



    BRIGHT_BLUE_LOWER_HUE = 0.44
    DESAT_BLUE_LOWER_HUE = 0.23
    DESAT_BLUE_UPPER_SAT = 0.25


    BRIGHT_RED_LOWER_SAT = 0.7
    PURPLE_RED_REGION_LOWER_HUE = 0.97
    PURPLE_RED_REGION_UPPER_HUE = 0.08
    PURPLE_RED_MIDDLE_SEPARATOR = 0.50


    def __init__(self,robot):
        self.robot = robot

    def start(self,dist):
        if dist > 70:
            print('No wall on this side')
            return []
        result, shape, color_name, col_hls, raw_col = self.process_image(self.robot.getPicture())
        if result == None:
            print('Nothing found.')
            return []
        cv2.imwrite('img/result.jpg',result)
        print(color_name + ' ' + shape + ': HLS' + str((int(col_hls[0]*360), int(col_hls[1]*100), int(col_hls[2]*100))))
        print('Raw HLS' + str((col_hls[0], col_hls[1], col_hls[2])))
        return [color_name, shape]

    def bgr_to_hls(self,color):
        temp = colorsys.rgb_to_hls(float(color[2])/255, float(color[1])/255, float(color[0])/255)
        return temp[0], temp[1], temp[2]


    def hls_to_bgr(self,color):
        temp = colorsys.hls_to_rgb(float(color[0]), float(color[1]), float(color[2]))
        return int(round(temp[2]*255, 1)), int(round(temp[1]*255, 1)), int(round(temp[0]*255, 1))


    # Get the background color
    def get_bg_color(self,img):
        pipet_x = len(img) - self.BACKGROUND_SAMPLE_DISTANCE
        pipet_y = self.BACKGROUND_SAMPLE_DISTANCE
        return img[pipet_x, pipet_y]


    # Make a mask of the background
    def get_background_mask(self,img, bg_color):
        blur = cv2.blur(img, (self.BLUR_SIZE, self.BLUR_SIZE))
        lower_bound = np.array([(1 - self.BACKGROUND_MASK_MUL_TOLERANCE) * bg_color[0] - self.BACKGROUND_MASK_DIFF_TOLERANCE,
                       (1 - self.BACKGROUND_MASK_MUL_TOLERANCE) * bg_color[1] - self.BACKGROUND_MASK_DIFF_TOLERANCE,
                       (1 - self.BACKGROUND_MASK_MUL_TOLERANCE) * bg_color[2] - self.BACKGROUND_MASK_DIFF_TOLERANCE],np.uint8)
        upper_bound = np.array([(1 + self.BACKGROUND_MASK_MUL_TOLERANCE) * bg_color[0] + self.BACKGROUND_MASK_DIFF_TOLERANCE,
                       (1 + self.BACKGROUND_MASK_MUL_TOLERANCE) * bg_color[1] + self.BACKGROUND_MASK_DIFF_TOLERANCE,
                       (1 + self.BACKGROUND_MASK_MUL_TOLERANCE) * bg_color[2] + self.BACKGROUND_MASK_DIFF_TOLERANCE],np.uint8)
        return cv2.inRange(blur, lower_bound, upper_bound)


    # calculate the area to crop away the top based on the background mask
    def get_top_crop_rect(self,masked_img):
        i = 0
        count = 0
        while i < len(masked_img):
            avg = np.average(masked_img[i])
            if avg > 240:
                count += 1
            if count == 2:
                return {'y1': i, 'y2': len(masked_img), 'x1': 0, 'x2': len(masked_img[0])}
            i += self.SKIP_STEP_SIZE


    # Helper function to crop an image
    def crop_rect(self,image, rect):
        return image[rect['y1']:rect['y2'], rect['x1']:rect['x2']]


    # Get the shape contour
    def get_shape_cont(self,img):
        (cont, _) = cv2.findContours(img.copy(), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        result = []
        for c in cont:
            area = cv2.contourArea(c)
            if self.CONTOUR_MIN_AREA < area < self.CONTOUR_MAX_AREA:
                result.append(c)
        return result


    def get_polys_from_contour(self,contour):
        epsilon_fine = self.POLYGON_FINE_EPSILON_MULTI * cv2.arcLength(contour, True)
        epsilon_rough = self.POLYGON_ROUGH_EPSILON_MULTI * cv2.arcLength(contour, True)
        fine = cv2.approxPolyDP(contour, epsilon_fine, True)
        rough = cv2.approxPolyDP(contour, epsilon_rough, True)
        return fine, rough


    # Actually detect the shape
    def detect_shape(self,poly_fine, poly_rough):
        points_fine = len(poly_fine)
        points_rough = len(poly_rough)
        if points_fine > 8:
            if cv2.isContourConvex(poly_fine):
                return 'circle'
            else:
                return 'star'
        else:
            if points_rough == 3:
                return 'triangle'
            else:
                return 'square'


    def detect_color(self,bg, image):
        raw_col = image[len(image)/2][len(image[0])/2]
        col_bgr = self.calibrate_colors(bg, raw_col)
        col_hls = self.bgr_to_hls(col_bgr)



        # Decide Red/Purle <-> Blue/Green
        # 0.72 -> 1 -> 0.14 ||| 0.14 -> 0.72

        if 0.14 < col_hls[0] < 0.72:
            print('Blue or Green')
            # Blue or Green
            if col_hls[0] > self.BRIGHT_BLUE_LOWER_HUE or (col_hls[0] > self.DESAT_BLUE_LOWER_HUE and col_hls[2] < self.DESAT_BLUE_UPPER_SAT):
                color_text = 'blue'
            else:
                color_text = 'green'
        else:
            print('Red or Purple')
            if col_hls[2] > self.BRIGHT_RED_LOWER_SAT:
                print("Bright")
                # Bright -> Likely Red
                if self.PURPLE_RED_MIDDLE_SEPARATOR > col_hls[0] or col_hls[0] > self.PURPLE_RED_REGION_LOWER_HUE:
                    # On the Red side or on the edge -> Red
                    color_text = 'red'
                else:
                    # On the Purple side -> Purple
                    color_text = 'purple'
            else:
                print("Dark")
                if self.PURPLE_RED_REGION_UPPER_HUE < col_hls[0] < self.PURPLE_RED_MIDDLE_SEPARATOR:
                    # On the Red side -> Red
                    color_text = 'red'
                else:
                    # On the edge or Purple side -> Purple
                    color_text = 'purple'
        return color_text, col_hls, raw_col


    def subtract_colors(self,col, diff):
        sub = [col[0] + diff[0], col[1] + diff[1], col[2] + diff[2]]
        max = 0
        for x in sub:
            if x > max:
                max = x

        if max > 255:
            scale = 255.0 / max
            return int(round(sub[0]*scale, 1)), int(round(sub[1]*scale, 1)), int(round(sub[2]*scale, 1))
        return sub


    def calibrate_colors(self,bg, col):
        diff = (self.BACKGROUND_BGR[0] - bg[0], self.BACKGROUND_BGR[1] - bg[1], self.BACKGROUND_BGR[2] - bg[2])
        #diff = (0,0,0)
        return self.subtract_colors(col, diff)

    def get_best_contour(self,contours):
        best = None
        best_score = 0
        for cont in contours:
            current_score = self.get_vertex_per_surface(cont)
            print('Testing contour:' + str(current_score))
            if current_score > best_score:
                best = cont
                best_score = current_score
        return best

    def get_vertex_per_surface(self,contour):
        epsilon_fine = self.POLYGON_FINE_EPSILON_MULTI * cv2.arcLength(contour, True)
        fine = cv2.approxPolyDP(contour, epsilon_fine, True)
        area = cv2.contourArea(contour)
        return float(area)/len(fine)

    def process_image(self,input):
        #src = cv2.imread(url)
        src = input.copy()
        cv2.imwrite('img/input.jpg',src)
        bg = self.get_bg_color(src)
        bg_mask = self.get_background_mask(src, bg)
        cv2.imwrite('img/mask.jpg',bg_mask)

        # Crop top part away
        top_crop_rect = self.get_top_crop_rect(bg_mask)
        bg_mask = self.crop_rect(bg_mask, top_crop_rect)
        src = self.crop_rect(src, top_crop_rect)

        # Detect shape
        contours = self.get_shape_cont(bg_mask)
        contour = self.get_best_contour(contours)
        if contour == None:
            return None, None, None, None, None
        (poly_fine, poly_rough) = self.get_polys_from_contour(contour)
        shape = self.detect_shape(poly_fine, poly_rough)

        # Bounding box
        x, y, w, h = cv2.boundingRect(contour)

        # Cropping
        mask = np.zeros((len(src), len(src[0]), 1), dtype=np.uint8)
        cv2.drawContours(mask, [contour], -1, (255, 255, 255), -1)
        result = cv2.bitwise_and(src, src, mask=mask)

        # Detect color
        color_name, col_hls, raw_col = self.detect_color(bg, result.copy()[y:y + h, x:x + w])

        # Draw Polygons
        cv2.polylines(result, [poly_fine], 1, (0, 127, 0), 3)
        cv2.polylines(result, [poly_rough], 1, (255, 255, 255), 1)

        # Crop
        result = result[y:y + h, x:x + w]

        return result, shape, color_name, col_hls, raw_col

    #
    # print("Loaded...")
    # images = ['img/test/green_circle_test.jpg',
    #           'img/test/blue_star_test.jpg',
    #           'img/test/blue_square_test.jpg',
    #           'img/test/blue_circle_test.jpg',
    #           'img/test/blue_triangle_test.jpg',
    #           'img/test/red_star_test.jpg',
    #           'img/test/red_triangle_test.jpg',
    #           'img/test/red_circle_test.jpg',
    #           'img/test/red_square_test.jpg',
    #           'img/test/purple_triangle_test.jpg',
    #           'img/test/purple_triangle_test_2.jpg',
    #           ]
    # scale = 50
    # palette = np.zeros((2*scale, len(images)*scale, 3), dtype=np.uint8)
    #
    # for i, img in enumerate(images):
    #     cropped, shape, color_name, col_hls, raw_col = process_image(img)
    #     col_bgr = hls_to_bgr(col_hls)
    #     print(color_name + ' ' + shape + ': HLS' + str((int(col_hls[0]*360), int(col_hls[1]*100), int(col_hls[2]*100))))
    #     cv2.rectangle(palette, (scale*i, 0), (scale*i + scale, scale), col_bgr , -1)
    #     cv2.rectangle(palette, (scale*i, scale), (scale*i + scale, 2*scale), (int(raw_col[0]), int(raw_col[1]), int(raw_col[2])), -1)
    #     cv2.imshow(color_name + ' ' + shape, cropped)
    #     cv2.waitKey(0)
    #     cv2.destroyAllWindows()
    #
    # cv2.imshow('Fixed and Original colors', palette)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    #
