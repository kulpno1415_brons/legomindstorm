import re
# class for describing the world of the robot
class World:
    #Constructor of World
    #   filename (String): location file containing information about the world
    def __init__(self, map):
        inputMatcher = "(\d)\s(\d)\n((\d[A-Z][\s\n])+)((\d+\w+/\w+.png\n?)*)"
        commentMatcher = "#[^\n]*\n"
        EmptyLineMatcher = "\n\s*\n"
        hiddenMatcher = "[?]"

        #Remove empty lines and comments and hidden objects
        map = re.sub(commentMatcher, "", map)
        map = re.sub(EmptyLineMatcher, "\n", map)
        map = re.sub(hiddenMatcher, "Nimages/black_circle.png", map)

        m = re.search(inputMatcher, map)
        x = int(m.group(1))
        y = int(m.group(2))
        blocksList = m.group(3)
        picturesList = m.group(5)

        #Read dimen
        self.dimensions = [x, y]

        #Read block information
        self.blocks = [[0 for n in range(x)] for n in range(y)]
        iterator = 0
        splitLine = "\s"
        for line in blocksList.splitlines():
            for i in re.split(splitLine, line):
                if (len(i) == 0 or iterator == x * y):
                    break
                btype = int(i[0])
                rotation = i[1]
                block_x,block_y = self.index_to_position(iterator)
                self.blocks[block_y][block_x] = Block(block_x,block_y,btype, rotation)
                iterator += 1

        #Read images
        splitLine = "(\d+)(\w)images/([a-z]+)_([a-z]+).png"
        for line in picturesList.splitlines():
            m = re.search(splitLine, line)
            blockNumber = int(m.group(1))
            orientation = m.group(2)
            color = m.group(3)
            figure = m.group(4)
            block_x,block_y = self.index_to_position(blockNumber)
            image = Image(color + ' ' + figure, self.blocks[block_y][block_x])
            self.blocks[block_y][block_x].set_image(image, orientation)
            
    #Get dimensions of the world
    def get_dimension(self):
        return self.dimensions

    def get_x_dimension(self):
        return self.dimensions[0]

    def get_y_dimension(self):
        return self.dimensions[1]

    #Get matrix of blocks
    def get_blocks(self):
        return self.blocks

    #Return a block with given coordinates
    def get_block(self, x, y):
        return self.blocks[y][x]

    #Return the position for the given index
    def index_to_position(self,index):
        x_size = self.get_x_dimension()
        x = index % x_size
        y = int(index / x_size)
        return [x,y]

    #Return the index for the given position
    def position_to_index(self,position):
        return position[0]+position[1]*self.get_x_dimension();
    
    # Returns the block that contains the given shape
    def get_block_from_shape(self, shape):
        for row in self.get_blocks():
            for block in row:
                if block.has_shape(shape):
                    return block
        return None

    # Input: two block instance
    # Return all blocks connected to a block
    def get_adjacent_blocks(self, block):
        result = []
        blocks = self.get_blocks()
        [x,y] = block.get_pos()
        walls = block.get_walls()

        if 'N' not in walls:
            result.append(blocks[y-1][x])
        if 'E' not in walls:
            result.append(blocks[y][x+1])
        if 'S' not in walls:
            result.append(blocks[y+1][x])
        if 'W' not in walls:
            result.append(blocks[y][x-1])
        return result

    #find shortest path from starting to ending position using an A* algorithm
    #   returns: array with shortest path, or empty array if there is no path
    def find_path(self,start_pos,goal_pos):
        start = self.get_blocks()[start_pos[1]][start_pos[0]]
        goal = self.get_blocks()[goal_pos[1]][goal_pos[0]]

        queue = [[start,[start,0,0,0],0,self.heuristic(start,goal)]]
        closed = []
        while len(queue) != 0 and queue[0][0] != goal:
            current = queue.pop(0)
            closed.append(current)
            neighbors = self.get_adjacent_blocks(current[0])
            for neighbor in neighbors:
                cost = current[2] + self.movementcost(current, neighbor)

                neighbor_in_queue = False
                for node in queue:
                    if node[0] == neighbor:
                        if node[2] > cost:
                            queue.remove(node)
                        else:
                            neighbor_in_queue = True
                        break;

                neighbor_in_closed = False
                for node in closed:
                    if node[0] == neighbor:
                        neighbor_in_closed = True
                        break;

                if not neighbor_in_queue and not neighbor_in_closed:
                    neighbor_node = [neighbor,current,cost,cost+self.heuristic(neighbor,goal)]
                    placed = False
                    for i in range(len(queue)):
                        if (queue[i][3] > neighbor_node[3]):
                            queue.insert(i,neighbor_node)
                            placed = True
                            break;
                    if not placed:
                        queue.append(neighbor_node)

        result = []
        if len(queue) == 0:
            return result

        curr = queue[0]
        while curr[0] != start:
            result.append(curr[0])
            curr = curr[1]
        result.append(start)
        result.reverse()

        return result

    #heuristic function for the A* algorithm
    #simple Manhattan Distance
    def heuristic(self, block, goal):
        result = 0
        goal_pos = goal.get_pos()
        block_pos = block.get_pos()
        result += abs(goal_pos[0] - block_pos[0])
        result += abs(goal_pos[1] - block_pos[1])
        return result

    #movement cost for the A* algorithm
    #straight path = 1
    #curve = 2
    def movementcost(self, current, next_block):
        prev_pos = current[1][0].get_pos()
        curr_pos = current[0].get_pos()
        next_pos = next_block.get_pos()

        if (prev_pos[0] == curr_pos[0] and curr_pos[0] == next_pos[0]) or (prev_pos[1] == curr_pos[1] and curr_pos[1] == next_pos[1]):
            return 1
        else:
            return 2

#class describing a single block of the world
class Block:
    #constructor with:
    #        x:  x coordinate of the block
    #        y:  y coordinate of the block
    #    btype:  block type
    # rotation:  rotation of the block
    def __init__(self, x, y, btype, rotation):
        self.position = [x,y]
        self.btype = btype
        self.rotation = rotation
        self.image = [None, '']
        self.walls = self.calculate_walls(btype,rotation)

    #internal function
    #used to calculate the walls given the block type and rotation
    def calculate_walls(self,type,rotation):
        if type == 0:
            temp = []
        elif type == 1:
            temp = ['N']
        elif type == 2:
            temp = ['W', 'E']
        elif type == 3:
            temp = ['N', 'W']
        elif type == 4:
            temp = ['N', 'W', 'E']
        elif ((type == 5) or (type == 6) or (type == 7)):
            temp = ['W', 'E']

        if rotation == 'N':
            return temp
        elif rotation == 'E':
            return self.rotate(temp, 1)
        elif rotation == 'S':
            return self.rotate(temp, 2)
        elif rotation == 'W':
            return self.rotate(temp, 3)

    #internal function
    #used to rotate walls of a block type
    def rotate(self, start, number):
        res = []
        lis = ['N', 'E', 'S', 'W']
        for elem in start:
            pos = lis.index(elem)
            pos += number
            if pos > 3:
                pos -= 4
            res.append(lis[pos])
        return res

    def get_pos(self):
        return self.position

    def get_type(self):
        return self.btype

    #get obstactle type
    def get_obstacle_name(self):
        names = ["normal", "normal", "normal", "normal", "normal", "ramp", "bottleneck", "seesaw"]
        return names[self.get_type()]

    def get_rotation(self):
        return self.rotation

    def set_image(self, image, orientation):
        self.image = [image, orientation]

    def get_image(self):
        return self.image[0]

    def get_image_orientation(self):
        return self.image[1]

    def get_walls(self):
        return self.walls

    def has_shape(self, shape):
        if not self.image[0]:
            return False
        return (self.image[0].get_shape() == shape)

class Image:
    def __init__(self, filename, block):
        if filename == "black circle":
            filename = "?"
        self.file = filename
        self.block = block

    def get_block(self):
        return self.block

    def get_shape(self):
        return self.file
