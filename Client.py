import requests
import re
import config

orientationNameToDegrees = {"N": 270, "E" : 0, "S": 90, "W" : 180}

class HttpClient():

    # Contruct Client to connect to a server with given name
    def __init__(self,servername):
        self.servername = servername

    # Retrieves world data from the server and puts this in a .txtfile
    def get_world(self):
        print self.servername + "/world"
        r = requests.get(self.servername + "/world")
        return r.text

    # Return the position and orientation of the team with the given team name
    # output format: (position(int), orientation(string))
    def get_position(self, teamname):
        r = requests.get(self.servername + "/position/" + teamname)
        output = r.text
        print output
        if output[0] == "?":
            position = output[0]
        else:
            print output[:-1]
            position = int(output[:2])
        orientation = output[-2]
        return (position, orientation)

    # Return the position and orientation of all teams
    # output format: dictionary that maps teamnames to (position, orientation)
    def get_position_all(self):
        positions = {}
        r = requests.get(self.servername + "/position/all")
        output = r.text
        print output
        match = re.findall(r'\D+\d+\D', output)
        positionsfound = match
        print positionsfound
        for i in positionsfound:
             match = re.search('(\D+)(\d+)(\D)', i)
             name = match.group(1)
             position = match.group(2)
             orientation = match.group(3)
             positions[name] = (int(position), orientation)
        return positions

    # Send a post request containing information about a teams position and orientation
    def post_position(self,teamname, position, orientation):
        payload = str(position)+orientation
        r = requests.post(self.servername + "/position/" + teamname, data=payload)

    # Send a post request to validate a spotted symbol
    # And return the response
    def post_symbol(self, teamname,  position, orientation, color_shape):
        payload = color_shape
        headers = {'user-agent': teamname}
        r = requests.post(self.servername + "/symbol/" + position + orientation, data=payload, headers=headers)
        if (r.status_code != 200):
            print "Bad Request"
            return r.text
        else:
            return r.text

    # Send A get request to retrieve all found symbols
    # Return a dictionary with discovered symbols and their position
    def get_symbol_all(self):
        symbols = {}
        r = requests.get(self.servername + "/symbol/all")
        output = r.text
        print output
        match = re.findall(r'\d\w+', output)
        for elem in match:
            match2 = re.search(r'(\d+[A-Z])(\w+)', elem)
            symbols[match2.group(2)] = match2.group(1)
        return symbols

    # Send a get request to retrieve the current score
    # Return a dictionary with discovered symbols
    def get_score_all(self):
        groups = {}
        r = requests.get(self.servername + "/score/all")
        output = r.text
        print output
        match = re.findall(r'\w+\d.\d', output)
        for elem in match:
            match2 = re.search(r'(\w+)(\d.\d)', elem)
            groups[match2.group(1)] = match2.group(2)
        return groups






