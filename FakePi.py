#import os
#if (os.uname()[4] == 'armv6l'):
#    from BrickPi import *
#else:
#    from FakePi import *

def BrickPiSetup():
    pass

PORT_A = 0
PORT_B = 1
PORT_C = 2
PORT_D = 3

PORT_1 = 0
PORT_2 = 1
PORT_3 = 2
PORT_4 = 3

def BrickPiUpdateValues():
    pass

def BrickPiSetupSensors():
    pass

class BrickPiStruct:
    Address = [1, 2]
    MotorSpeed = [0] * 4

    MotorEnable = [0] * 4

    EncoderOffset = [None] * 4
    Encoder = [None] * 4

    Sensor = [None] * 4
    SensorArray = [[None] * 4 for i in range(4)]
    SensorType = [0] * 4
    SensorSettings = [[None] * 8 for i in range(4)]

    SensorI2CDevices = [None] * 4
    SensorI2CSpeed = [None] * 4
    SensorI2CAddr = [[None] * 8 for i in range(4)]
    SensorI2CWrite = [[None] * 8 for i in range(4)]
    SensorI2CRead = [[None] * 8 for i in range(4)]
    SensorI2COut = [[[None] * 16 for i in range(8)] for i in range(4)]
    SensorI2CIn = [[[None] * 16 for i in range(8)] for i in range(4)]
    Timeout = 0

BrickPi = BrickPiStruct()

class GPIOStruct:
    def setmode(self,dunnowhat):
        pass
    def setup(self, a,b,pull_up_down=1):
        pass
    def output(trigger, boolean):
        pass
    def intput(a):
        return 0;

GPIO = GPIOStruct()
