import math
import Interface
import time
class PositionTracker():
    PROCESS_DISTANCE = 0.1
    MIN_POINTS = 10
    BLOCK_SIZE = 0.8
    ROTATE_OFFSET = {'left': 90, 'right': -90}
    MIN_CORRELATION = 0.15
    EPSILON = 0.0001

    def __init__(self, robot):
        self.robot = robot
        self.side_points = []
        self.front_points = []
        self.side_wall = self.determine_wall('side')
        self.front_wall = self.determine_wall('front')
        self.distance_since_side_process = 0
        self.distance_since_front_process = 0
        self.last_ticks = robot.get_motor_ticks()
        self.moved = True
        self.active = True

    def execute(self):
        self.track_pins()
        if not self.active:
            return
        self.update_walls()
        if self.moved:
            self.track_sensors()
        self.process_side_sensor()
        self.process_front_sensor()
    def start(self):
        self.active = True
        self.clear()

    def stop(self):
        self.active = False
        self.clear()

    def track_pins(self):
        if self.robot.target_speed[0]*self.robot.target_speed[1] < 0:
            self.last_ticks = self.robot.get_motor_ticks()
            return
        ticks = self.robot.get_motor_ticks()
        diff_ticks = [ticks[0] - self.last_ticks[0], ticks[1] - self.last_ticks[1]]
        diff_dist = self.robot.get_motor_distance(diff_ticks)

        if abs(diff_dist[0]) > abs(diff_dist[1]):
            big_l = float(diff_dist[0])
            small_l = float(diff_dist[1])
        else:
            big_l = float(diff_dist[1])
            small_l = float(diff_dist[0])

        angle = ((big_l - small_l) / self.robot.wheelBase) % (2*math.pi)
        if angle != 0:
            radius = abs(small_l*self.robot.wheelBase / (big_l - small_l))
            delta = [math.sin(angle)*(radius + self.robot.wheelBase/2),
                     (math.cos(angle)-1)*(radius + self.robot.wheelBase/2)]
        else:
            delta = [diff_dist[0], 0.0]
        robot_angle = ((self.robot.get_orientation()) * math.pi / 180)
        corrected_delta = [(math.cos(robot_angle) * delta[0] - math.sin(robot_angle) * delta[1]),
                           (math.sin(robot_angle) * delta[0] + math.cos(robot_angle) * delta[1])]
        self.robot.position = [self.robot.position[0] + corrected_delta[0], self.robot.position[1] + corrected_delta[1]]
        distance_moved = math.sqrt(math.pow(corrected_delta[0], 2) + math.pow(corrected_delta[1], 2))
        self.distance_since_side_process += distance_moved
        self.distance_since_front_process += distance_moved
        if distance_moved != 0:
            self.moved = True
        else:
            self.moved = False
        self.last_ticks = ticks
        #Interface.log("Tracked pins")

    def track_sensors(self):
        orientation = self.robot.get_orientation()
        look_direction = float(orientation - self.ROTATE_OFFSET[self.robot.lookDirection]) * math.pi / 180
        # Side Sensor
        side_distance = float(self.robot.get_side_distance()) / 100 + 0.08
        if side_distance < 1 and side_distance != 0:
            side_look_vector = [math.cos(look_direction), math.sin(look_direction)]
            #Interface.log(side_look_vector)
            measured_side_point = [self.robot.position[0] + side_distance*side_look_vector[0],
                                   self.robot.position[1] + side_distance*side_look_vector[1]]
            if len(self.side_points) == 0 or measured_side_point != self.side_points[-1]:
                self.side_points.append(measured_side_point)
            #Interface.log(measured_side_point)


        # Front Sensor
        front_distance = float(self.robot.get_forward_distance()) / 100 + 0.05
        orientation_rad = orientation * math.pi / 180
        if front_distance != 0 and front_distance < 0.8:
            front_look_vector = [math.cos(orientation_rad), math.sin(orientation_rad)]
            measured_front_point = [self.robot.position[0] + front_distance*front_look_vector[0],
                                   self.robot.position[1] + front_distance*front_look_vector[1]]
            if len(self.front_points) == 0 or measured_front_point != self.front_points[-1]:
                self.front_points.append(measured_front_point)

        #Interface.log("Tracked Sensors")

    def clear(self):
        self.clear_front()
        self.clear_side()
        self.last_ticks = self.robot.get_motor_ticks()

    def clear_side(self):
        self.side_points = []
        self.side_wall = self.determine_wall('side')
        self.distance_since_side_process = 0

    def clear_front(self):
        self.front_points = []
        self.front_wall = self.determine_wall('front')
        self.distance_since_front_process = 0


    def determine_wall(self, sensor):
        orientation = self.robot.get_orientation()
        block = self.robot.get_block(self.robot.position[0], self.robot.position[1])
        block_center = [block.get_pos()[0] * self.BLOCK_SIZE + self.BLOCK_SIZE/2,
                        block.get_pos()[1] * self.BLOCK_SIZE + self.BLOCK_SIZE/2]
        if sensor == 'side':
            look_direction = orientation - self.ROTATE_OFFSET[self.robot.lookDirection]
        else:
            look_direction = orientation
        look_direction = (look_direction % 360) * math.pi / 180
        if look_direction > math.pi:
            look_direction -= 2*math.pi
        #print(look_direction)
        block_offset = [self.BLOCK_SIZE * block.get_pos()[0], self.BLOCK_SIZE * block.get_pos()[1]]
        position_in_block = [self.robot.position[0] - block_offset[0],
                             self.robot.position[1] - block_offset[1]]
        top_left = [-position_in_block[0], -position_in_block[1]]
        top_right = [self.BLOCK_SIZE - position_in_block[0], -position_in_block[1]]
        bottom_left = [-position_in_block[0], self.BLOCK_SIZE - position_in_block[1]]
        bottom_right = [self.BLOCK_SIZE - position_in_block[0], self.BLOCK_SIZE - position_in_block[1]]

        #print([top_left,top_right,bottom_left,bottom_right])

        top_left_dir = math.atan2(top_left[1],top_left[0])
        top_right_dir = math.atan2(top_right[1],top_right[0])
        bottom_left_dir = math.atan2(bottom_left[1],bottom_left[0])
        bottom_right_dir = math.atan2(bottom_right[1],bottom_right[0])

        # print([top_left_dir,
        #        top_right_dir,
        #        bottom_left_dir,
        #        bottom_right_dir])
        if look_direction >= 0:
            if look_direction < bottom_right_dir:
                return ['E', block_center[0] + self.BLOCK_SIZE / 2,
                    block_center[1] - self.BLOCK_SIZE/2, block_center[1] + self.BLOCK_SIZE/2]
            elif look_direction <bottom_left_dir:
                return ['S', block_center[1] + self.BLOCK_SIZE / 2,
                    block_center[0] - self.BLOCK_SIZE/2, block_center[0] + self.BLOCK_SIZE/2]
            else:
                return ['W', block_center[0] - self.BLOCK_SIZE / 2,
                    block_center[1] - self.BLOCK_SIZE/2, block_center[1] + self.BLOCK_SIZE/2]

        else:
            if look_direction > top_right_dir:
                return ['E', block_center[0] + self.BLOCK_SIZE / 2,
                    block_center[1] - self.BLOCK_SIZE/2, block_center[1] + self.BLOCK_SIZE/2]
            elif look_direction > top_left_dir:
                return ['N', block_center[1] - self.BLOCK_SIZE / 2,
                    block_center[0] - self.BLOCK_SIZE/2, block_center[0] + self.BLOCK_SIZE/2]
            else:
                return ['W', block_center[0] - self.BLOCK_SIZE / 2,
                    block_center[1] - self.BLOCK_SIZE/2, block_center[1] + self.BLOCK_SIZE/2]

    def update_walls(self):
        side_wall = self.determine_wall('side')
        if self.side_wall != side_wall and not self.is_continuous(self.side_wall,side_wall):
            self.clear_side()
        self.side_wall = side_wall

        front_wall = self.determine_wall('front')
        if self.front_wall != front_wall and not self.is_continuous(self.front_wall,front_wall):
            Interface.log("Wall has changed")
            self.clear_front()
        self.front_wall = front_wall

    def is_in_processable_block(self):
        block = self.robot.get_block(self.robot.position[0], self.robot.position[1])
        if block.getType() in [1,2,3,4]:
            return True

    def process_side_sensor(self):
        if len(self.side_points) < self.MIN_POINTS or self.distance_since_side_process < self.PROCESS_DISTANCE:
            return
        if not self.is_in_processable_block():
            return

        stat_data = self.calc_stat_data(self.side_points)
        #Interface.log(stat_data)
        if abs(stat_data['r']) < self.MIN_CORRELATION:
            Interface.log("Correlation too low: " + str(stat_data['r']))
            self.clear_side()
            return

        # trend line
        slope = stat_data['r'] * (stat_data['sigma'][1] / stat_data['sigma'][0])
        offset = stat_data['mean'][1] - slope * stat_data['mean'][0] # Not needed, maybe use to visualize?
        wall = self.determine_wall('side')

        # angle_diff in rad
        angle = math.atan(slope)
        angle_correction = 0
        if wall[0] == 'E' or wall[0] == 'W':
            if angle > 0:
                angle_correction = math.pi/2 - angle
            else:
                angle_correction = -math.pi/2 - angle
        elif wall[0] == 'N' or wall[0] == 'S':
            if angle > 0:
                angle_correction = -angle
            else:
                angle_correction = -angle

        print("Correcting:" + str(angle_correction*180 / math.pi) + " Degrees")
        #print("Angle:" + str(angle * 180 / math.pi) + " Degrees")
        # Apply angle correction
        self.robot.start_angle += angle_correction * 180 / math.pi

        # Correct the position
        origin = self.robot.position
        rotated_points = self.transform_data(self.side_points, origin, angle_correction)
        rotated_stat_data = self.calc_stat_data(rotated_points)
        self.front_points = self.transform_data(self.front_points,origin,angle_correction)

        error_x = 0
        error_y = 0
        if wall[0] == 'E' or wall[0] == 'W':
            error_x = rotated_stat_data['mean'][0] - wall[1]
        elif wall[0] == 'N' or wall[0] == 'S':
            error_y = rotated_stat_data['mean'][1] - wall[1]

        print("Correcting: (" + str(error_x) + ","+str(error_y) + ")")
        # Apply translation correction
        self.robot.position[0] -= error_x
        self.robot.position[1] -= error_y
        self.clear_side()

    def process_front_sensor(self):
        if len(self.front_points) < self.MIN_POINTS or self.distance_since_side_process < self.PROCESS_DISTANCE:
            return
        if not self.is_in_processable_block():
            return
        stat_data = self.calc_stat_data(self.front_points)
        #Interface.log(stat_data)
        if abs(stat_data['r']) < self.MIN_CORRELATION:
            Interface.log("Correlation too low: " + str(stat_data['r']))
            self.clear_front()
            return

        error_x = 0
        error_y = 0
        if self.front_wall[0] == 'E' or self.front_wall[0] == 'W':
            error_x = stat_data['mean'][0] - self.front_wall[1]
        elif self.front_wall[0] == 'N' or self.front_wall[0] == 'S':
            error_y = stat_data['mean'][1] - self.front_wall[1]

        print("Correcting: (" + str(error_x) + ","+str(error_y) + ")")
        # Apply translation correction
        self.robot.position[0] -= error_x
        self.robot.position[1] -= error_y
        self.clear_front()

    def is_continuous(self,wall1,wall2):
        if wall1[0] != wall2[0]:
            return False
        if wall1[1] != wall2[1]:
            return False
        if self.EPSILON > abs(wall1[2] - wall2[3]) or self.EPSILON > abs(wall1[3] - wall2[2]):
            return True
        return False


    def calc_stat_data(self, data):
        mean = [0.0, 0.0]
        for point in data:
            mean[0] += point[0]
            mean[1] += point[1]
        mean[0] /= len(data)
        mean[1] /= len(data)

        sigma = [0.0, 0.0]
        for point in data:
            sigma[0] += math.pow(point[0] - mean[0], 2)
            sigma[1] += math.pow(point[1] - mean[1], 2)
        sigma[0] = math.sqrt(sigma[0]/(len(data) - 1))
        sigma[1] = math.sqrt(sigma[1]/(len(data) - 1))

        prod = 0.0
        if sigma[0] == 0 or sigma[1] == 0:
            pass
        else:
            for i in range(0, len(data)):
                prod += ((data[i][0] - mean[0])/sigma[0]) * ((data[i][1] - mean[1])/sigma[1])
            r = prod/(len(data) - 1)
        return {'mean': mean, 'sigma': sigma, 'r': r}

    def transform_data(self, data, origin, rotation):
        output = []
        for point in data:
            translated = [point[0] - origin[0], point[1] - origin[1]]
            rotated = [(math.cos(rotation) * translated[0] - math.sin(rotation) * translated[1]),
                       (math.sin(rotation) * translated[0] + math.cos(rotation) * translated[1])]
            output.append([rotated[0] + origin[0], rotated[1] + origin[1]])
        return output