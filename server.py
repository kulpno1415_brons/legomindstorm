from flask import Flask, request, abort, make_response
import re
import config
import World

app = Flask(__name__)
locations = {"brons": "??", "koper": "??", "paars": "??", "blauw": "??", "rood": "??", "groen": "??", "geel": "??", "indigo": "??", "zilver": "??", "goud": "??", "ijzer": "??", "platinum": "??"}
scores = {"brons": 0.0, "koper": 0.0, "paars": 0.0, "blauw": 0.0, "rood": 0.0, "groen": 0.0, "geel": 0.0, "indigo": 0.0, "zilver": 0.0, "goud": 0.0, "ijzer": 0.0, "platinum": 0.0}
guesses = {"brons": {}, "koper": {}, "paars": {}, "blauw": {}, "rood": {}, "groen": {}, "geel": {}, "indigo": {}, "zilver": {}, "goud": {}, "ijzer": {}, "platinum": {}}
colors = ["blue", "green", "red", "purple"]
shapes = ["circle", "triangle", "square", "star"]
discovered = {}
positionRegex = re.compile("^(\d*)([A-Z])$")
orientationRegex = re.compile("[NESW]")
color_shape_regex = re.compile("([a-z]+)_([a-z]+)")
world = World.World(open(config.MAP_FILE).read())
x,y = world.get_dimension()
numberOfBlocks = x*y

@app.route("/world")
def get_world():
    return open(config.MAP_FILE, "r").read()

@app.route("/name", methods=["GET", "POST"])
def name():
    if request.method == "GET":
        return "test\n"
    if request.method == "POST":
        clients[request.remote_addr] = request.form("name")
        return "Your new name is " + request.form("name") + "\n"

@app.route("/position/<string:name>", methods=["GET", "POST"])
def position(name):
    if request.method == "GET":
        output = ""
        if name == "all":
            robots = []
            for n, p in locations.items():
                if not(p == "??"):
                    robots.append(n + p)
            output += " ".join(robots) + "\n"
        else:
            if name in locations:
                output = locations[name] + "\n"
            else:
                return make_response("Invalid team: " + name + "\n", 400)
        return output
    if request.method == "POST":
        if not(name in locations):
            return make_response("Invalid team: " + name + "\n", 400)
        data = request.get_data()
        match = positionRegex.match(data)
        if not match:
            return make_response("Bad Format: " + data + "\n", 400)
        if not validPosition(match.group(1)):
            return make_response("Invalid position: " + match.group(1) + "\n", 400)
        if not validOrientation(match.group(2)):
            return make_response("Invalid orientation: " + match.group(2) + "\n", 400)
        locations[name] = data
        return "OK\n"

@app.route("/symbol/<string:path>", methods=["GET", "POST"])
def symbol(path):
    if request.method == "POST":

        team = request.headers.get("User-Agent")
        if not locations.has_key(team):
            return make_response("Invalid team: " + team + "\n", 400)

        position_regex_match = positionRegex.match(path)
        if not position_regex_match:
            return make_response("Bad Format: " + path + "\n" , 400)
        fullPosition = position_regex_match.group(0)

        position = position_regex_match.group(1)
        if not validPosition(position):
            return make_response("Invalid position: " + position + "\n", 400)

        orientation = position_regex_match.group(2)
        if not validOrientation(orientation):
            return make_response("Invalid orientation: " + orientation + "\n", 400)

        color_shape = request.get_data()
        color_shape_match = color_shape_regex.match(color_shape)
        if not color_shape_match: #invalid color
            return make_response("Invalid Color: " + color_shape + "\n", 400)

        color = color_shape_match.group(1)
        if not validColor(color):
            return make_response("Invalid Color: " + color + "\n", 400)

        shape = color_shape_match.group(2)
        if not validShape(shape):
            return make_response("Invalid shape: " + shape + "\n", 400)

        team_position_match = positionRegex.match(locations[team])
        team_position = team_position_match.group(1)
        if position != team_position:
            return make_response("Robot not on tile: " + locations[team] + "\n", 400)

        #TODO No symbol at

        if correctGuess(position, orientation, color, shape):
            if (alreadyGuessed(fullPosition)):
                return "TOOLATE\n"
            score = 0.0
            if not guesses[team].has_key(fullPosition): # if guesses == 0
                score = 1.0
                discovered[fullPosition] = color + "_" + shape
            elif guesses[team][fullPosition] == 1:
                score = 0.5
                discovered[fullPosition] = color + "_" + shape
            elif guesses[team][fullPosition] > 1:
                score = 0.0
                discovered[fullPosition] = color + "_" + shape
            scores[team] += score
            return str(score) + "\n"
        else:
            if guesses[team].has_key(fullPosition): # guesses != 0
                guesses[team][fullPosition] += 1
            else:                                   # guesses == 0
                guesses[team][fullPosition] = 1
            return "WRONG\n"
    elif request.method == "GET":
        if path != "all":
            return make_response("Invalid request: " + path + "\n", 400)
        responseList = []
        for pos, colorshape in discovered.iteritems():
            responseList.append(pos+colorshape)
        return " ".join(responseList) + "\n"
    else:
        return make_response("Invalid method: " + request.method + "\n", 400)

@app.route("/score/all", methods=["GET"])
def get_all_scores():
    responseList = []
    for team, score in scores.iteritems():
        responseList.append(team + str(score))
    return " ".join(responseList) + "\n"


def validPosition(position):
    if len(position) > 0:
        return int(position) < numberOfBlocks
    return False

def validOrientation(data):
    if orientationRegex.match(data):
        return True
    else:
        return False

def validColor(color):
    return color in colors

def validShape(shape):
    return shape in shapes

def correctGuess(location, orientation, color, shape):
    col,row = world.index_to_position(int(location))
    block = world.get_block(col,row)
    _orientation = block.get_image_orientation()
    return (orientation == _orientation and block.has_shape(color + " " + shape))

def alreadyGuessed(fullPosition):
    return discovered.has_key(fullPosition)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5001, debug=True)
