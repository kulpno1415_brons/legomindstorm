import signal
import os
import Interface
from BrickPi import *
import camera

def interruptHandler(signal, frame):
    print("\nStopping..")
    os._exit(0)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, interruptHandler)
    BrickPiSetup()
    # camera = camera.CameraStream()
    # camera.start()
    update = Interface.UpdateThread()
    update.start()

    server = Interface.PIServer(('0.0.0.0', 5000), Interface.PIServerHandler)
    print("loaded")
    server.serve_forever()
    print("stopping main thread")
