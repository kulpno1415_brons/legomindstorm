import SocketServer
import socket
import json
import re
import threading
import Robot
import time
import config
import Client
from BrickPi import *


class PIServer(SocketServer.ThreadingTCPServer):
    allow_reuse_address = True


# ###############################################################


class PIServerHandler(SocketServer.BaseRequestHandler):
    def sendSensorData(self):
        ret = {'return': 'data', 'sonarLego': Robot.robot.get_side_distance(),
               'sonarPCB': Robot.robot.get_forward_distance()}
        self.sendData(ret)

    def get_position_all_UI(self):
        positions = Robot.robot.httpClient.get_position_all()
        output = []
        for k,v in positions.iteritems():
            if (k != "brons"):
                blockpos = Robot.robot.calculate_coordinates(v[0])
                output.append({"name": k, "x": blockpos[0], "y": blockpos[1], "orientation": Client.orientationNameToDegrees[v[1]]})
        return output

    def sendMapData(self):
        brons = {"name": "brons", "x": Robot.robot.get_position()[0], "y": Robot.robot.get_position()[1], "orientation": Robot.robot.get_orientation()}
        if config.USING_API_SERVER:
            nowTime = time.time()
            if  nowTime - self.lastRequestTime > 1 or self.carCache == None:
                self.carCache = self.get_position_all_UI()
                Robot.robot.post_own_position()
                self.lastRequestTime = nowTime
            cars = list(self.carCache)
            cars.append(brons)
        else:
            cars = [brons]

        path_pos = []
        for block in Robot.robot.path:
            path_pos.append(block.get_pos())

        ret = {'return': 'data',
               #'position' : Robot.robot.position,
               #'orientation' : Robot.robot.getOrientation(),
               'cars' : cars,
               'map' :self.getMap(),
               'target' : Robot.robot.target,
               'path': path_pos,
               'side_points' : Robot.robot.position_tracker.side_points,
               'front_points' : Robot.robot.position_tracker.front_points,
               'actions': Robot.robot.commands}
        self.sendData(ret)

    def sendLogData(self):
        if logger.hasLogs():
            ret = {'return': 'data'}
            ret['log'] = logger.getLog()
            logger.wipe()
            self.sendData(ret)

    def getMap(self):
        result = {}
        result['x'] = Robot.robot.get_world().get_x_dimension()
        result['y'] = Robot.robot.get_world().get_y_dimension()
        result['blocks'] = []
        for row in Robot.robot.get_world().get_blocks():
            for block in row:
                temp = {}
                temp['walls'] = block.get_walls()
                temp['type'] = block.get_type()
                temp['image_orientation'] = block.get_image_orientation()
                result['blocks'].append(temp)
        return result

    def sendData(self, data):
        try:
            self.request.sendall(protocolFormat(json.dumps(data)))
        except Exception as e:
            #log("Error while sending data")
            #log(e)
            pass

    def doCommand(self, command):
        pass

    def setup(self):
        self.sendingSensorData = False
        self.sendingLogs = False
        self.sendingMap = False
        self.request.settimeout(0.010)
        self.commandRegex = re.compile('^\|({[^\|]+})\|(.*)')
        self.lastRequestTime = time.time()
        self.carCache = None

    def handle(self):
        while True:
            if (self.sendingSensorData):
                self.sendSensorData()
            if (self.sendingLogs):
                self.sendLogData()
            if (self.sendingMap):
                self.sendMapData()
            try:
                raw = self.request.recv(1024)
                if (raw == ""):
                    log("no data, breaking")
                    break

                match = self.commandRegex.match(raw.strip())
                while match:
                    ret = {'return': 'ok'}
                    incoming = json.loads(match.group(1))
                    match = self.commandRegex.match(match.group(2))
                    # log(incoming)
                    # process the data:
                    cmd = incoming['command']
                    if cmd == 'data':
                        if incoming['type'] == 'sensor':
                            self.sendingSensorData = True
                        elif incoming['type'] == 'map':
                            ret = {'return': 'data'}
                            ret['map'] = self.getMap()
                            self.sendingMap = True
                        elif incoming['type'] == 'log':
                            self.sendingLogs = True

                    elif cmd == 'move':
                        direction = incoming['direction']

                        if direction == 'forward':
                            Robot.robot.move(Robot.robot.drive.throttle, Robot.robot.drive.throttle)
                        elif direction == 'backward':
                            Robot.robot.move(-Robot.robot.drive.throttle, -Robot.robot.drive.throttle)
                        elif direction == 'rotate_left':
                            Robot.robot.move(Robot.robot.drive.throttle/2, -Robot.robot.drive.throttle/2)
                        elif direction == 'rotate_right':
                            Robot.robot.move(-Robot.robot.drive.throttle/2, Robot.robot.drive.throttle/2)
                        elif direction == 'forward_left':
                            Robot.robot.move(Robot.robot.drive.throttle/2, Robot.robot.drive.throttle)
                        elif direction == 'forward_right':
                            Robot.robot.move(Robot.robot.drive.throttle, Robot.robot.drive.throttle/2)
                        elif direction == 'stop':
                            Robot.robot.move(0,0)

                        ret['return'] = 'Moving: ' + direction
                    elif cmd == 'rotate':
                        degrees = incoming['degrees']
                        Robot.robot.rotateDegrees(degrees)
                    elif cmd == 'drive':
                        distance = incoming['distance']
                        Robot.robot.drive_distance(distance)
                    elif cmd == 'throttle':
                        throttle = int(incoming['value'])
                        Robot.robot.set_throttle(throttle)
                        ret['return'] = 'Throttle: ' + str(throttle)
                    elif cmd == 'follow':
                        Robot.robot.follow(incoming['direction'])
                    elif cmd == 'abort':
                        Robot.robot.abort()
                    elif cmd == 'nav':
                        Robot.robot.nav(Robot.robot.get_world().index_to_position(incoming['goal']))
                    elif cmd == 'reset':
                        print(incoming)
                        Robot.robot.reset(Robot.robot.get_world().index_to_position(incoming['position']), incoming['orientation'])
                    elif cmd == 'nav_coord':
                        Robot.robot.nav_coord(Robot.robot.get_world().index_to_position(incoming['goal']))
                    else:
                        ret['return'] = 'Unknown command:' + cmd

                    # send some 'ok' back
                    # log(json.dumps(ret))
                    ticks = Robot.robot.get_motor_ticks()
                    self.sendData(ret)
            except socket.timeout as e:
                pass
                # except Exception as e:
                # log(e)
                # break


# ###############################################################



class UpdateThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.name = "updateThread"
        self.lastTicks = None

    def run(self):
        while True:
            Robot.lock.acquire()
            BrickPiUpdateValues()
            Robot.lock.release()
            Robot.robot.update_position()
            Robot.robot.update_movement_speed()
            # self.correctBias()
            time.sleep(0.05)

    def correctBias(self):
        if not Robot.robot.straight:
            return
        if self.lastTicks == None:
            self.lastTicks = Robot.robot.get_motor_ticks()
            return
        ticks = Robot.robot.get_motor_ticks()
        diff = [ticks[0] - self.lastTicks[0], ticks[1] - self.lastTicks[1]]
        if diff[0] > diff[1]:
            Robot.robot.steeringBias -= 0.01
        elif diff[0] < diff[1]:
            Robot.robot.steeringBias += 0.01
        self.lastTicks = ticks


class DebugLog():
    def __init__(self):
        self._log = []
        self.lock = threading.Lock()
        self._hasLogs = False

    def getLog(self):
        self.lock.acquire()
        output = list(self._log)
        self.lock.release()
        return output

    def wipe(self):
        self.lock.acquire()
        self._log = []
        self._hasLogs = False
        self.lock.release()

    def log(self, message):
        print(str(message))
        self.lock.acquire()
        self._log.append(str(time.strftime("%H:%M:%S", time.gmtime()) + " " + str(message)))
        self._hasLogs = True
        self.lock.release()

    def hasLogs(self):
        self.lock.acquire()
        output = self._hasLogs
        self.lock.release()
        return output


logger = DebugLog()


def log(message):
    logger.log(message)


def protocolFormat(message):
    return "|" + str(message) + "|"


if __name__ == "__main__":
    server = PIServer(('0.0.0.0', 5000), PIServerHandler)
    server.serve_forever()
