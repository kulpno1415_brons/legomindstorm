import numpy as np
import cv2
import cv

class ShapeRecognition():
        def __init__(self, robot):
            self.robot = robot

        def getcolouredshape(self,Distance):
                meanColor = []
                img = self.robot.get_picture()
                cv2.imwrite('original.jpg',img)
                #voor de kleurendetectie moeten HSV kleuren ipv BGR kleuren gebruikt worden
                #dit is gemakkelijker voor het herkennen van kleuren door het kleurbereik
                hsv = cv2.cvtColor(img,cv.CV_BGR2HSV)
                meanColor = cv2.mean(hsv)
                print(str(meanColor))

                #determining how much pixels are above the edge:
                invalidPixels = (Distance*400)/120
                print('distance = ' + str(Distance) + ' ,skipping '+ str(invalidPixels) + ' pixels')

                #defining the colors, some colors are depending on the mean image color
                #to keep track of the lighting conditions.
                        
                #Define Red1
                saturation = 135
                if(meanColor[1]>130):
                        saturation = 0.9*meanColor[1]
                if(meanColor[1]>145):
                        saturation = 1.1*meanColor[1]
                lower_red1 = np.array([170,saturation, 75], dtype=np.uint8)
                upper_red1 = np.array([180,255, 255], dtype=np.uint8)
                red1 = [lower_red1, upper_red1, 'red']

                #Define Red2
                lower_red2 = np.array([0,140, meanColor[2]], dtype=np.uint8)
                upper_red2 = np.array([10,255, 255], dtype=np.uint8)
                red2 = [lower_red2, upper_red2, 'red']

                #Define Green
                saturation = 165
                hue = 24
                if meanColor[1]>135:
                    saturation = 1.2*meanColor[1]
                if meanColor[1]>145:
                    saturation = 1.25*meanColor[1]
		if meanColor[1]>150:
                    saturation = 1.45*meanColor[1]
		if meanColor[1]>155:
                    saturation = 1.55*meanColor[1]

                #if meanColor[1]>20:
                        #hue = 28
                lower_green = np.array([hue, 55, 0], dtype=np.uint8)
                upper_green = np.array([80, saturation,220], dtype=np.uint8)
                green = [lower_green, upper_green, 'green']

                #Define Blue
                lower_blue = np.array([90, 5, 60], dtype=np.uint8)
                upper_blue = np.array([155, 255, 255], dtype=np.uint8)
                blue = [lower_blue, upper_blue, 'blue']

                #define purple
                lower_purple = np.array([110,40,0], dtype = np.uint8)
                upper_purple = np.array([175,255,255], dtype = np.uint8)
                purple = [lower_purple,upper_purple, 'purple']


                #because red is an overlapping color (170 degrees to 10 degrees) we make
                #a separate mask for red
                mask = cv2.inRange(hsv,red1[0],red1[1])
                mask2 = cv2.inRange(hsv,red2[0],red2[1])
                cv2.bitwise_or(mask,mask2,mask)
                cv2.imwrite(str('mask_red.jpg'),mask)
                contours,h = cv2.findContours(mask,1,2)
                approx =[0]
                distance = 0
                for cnt in contours:
                                approx1 = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt,True),True)
                                reference =[]
                                for point in approx1:
					if(point[0][1]>invalidPixels):
						reference = point[0]
                                if(len(reference) == 2):
                                        for point in approx1:
                                                distance = np.linalg.norm(point[0]-reference,1)
                                                if((point[0][0] == 0) & (distance>100)):
                                                        print('I detected a ' + str(color[2]) + ' shape, but it is to far right')
                                                        return ['red','right']
                                                if(point[0][0] == 719):
                                                        return ['red','left']
                                                        print('I detected a ' + str(color[2]) + ' shape, but it is to far left')
                                                if((point[0][1] > 475) & (distance>70)):
                                                        return [color[2],'too_close']
                                                if ((distance>120) & (point[0][1] >invalidPixels)):
                                                        print (distance)
                                                        approx = approx1
                                if ((len(approx)>=9) & (len(approx)<12)):
                                        return ['red','star']
                                        print (str(color[2]) +" star")
                                elif len(approx)==3:
                                        return ['red','triangle']
                                        print ("red triangle")
                                elif len(approx)==4:
                                        return ['red','square']
                                        print ("red square")
                                elif len(approx) > 11:
                                        return ['red','circle']
                                        print ("red circle")
                #we can make a loop to check the rest of the colors.
                colors = [green,blue,purple]

                for color in colors:
			print(str(color[2]))
                        mask = cv2.inRange(hsv,color[0],color[1])
                        contours,h = cv2.findContours(mask,1,2)
                        approx =[0]
                        distance = 0
                        for cnt in contours:
                                approx1 = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt,True),True)
                                reference =[]
                                for point in approx1:
					if(point[0][1]>invalidPixels):
						reference = point[0]
                                if(len(reference) == 2):
                                        for point in approx1:
                                                distance = np.linalg.norm(point[0]-reference,1)
                                                if((point[0][0] == 0) & (distance>100)):
                                                        print('I detected a ' + str(color[2]) + ' shape, but it is to far right')
                                                        return [color[2],'right']
                                                if(point[0][0] == 719):
                                                        return [color[2],'left']
                                                        print('I detected a ' + str(color[2]) + ' shape, but it is to far left')
                                                if((point[0][1] > 475) & (distance>70)):
                                                        return [color[2],'too_close']
                                                if ((distance>120) & (point[0][1] >invalidPixels)):
                                                        approx = approx1
                        if ((len(approx)>=9) & (len(approx)<11)):
                                return [color[2],'star']
                                print (str(color[2]) +" star")
                        elif len(approx)==3:
                                return [color[2],'triangle']
                                print (str(color[2]) + " triangle")
                        elif len(approx)==4:
                                return [color[2],'square']
                                print (str(color[2]) + " square")
                        elif len(approx) >= 11:
                                return [color[2],'circle']
                                print (str(color[2]) + " circle")
		return[]
