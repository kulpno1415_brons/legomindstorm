from BrickPi import *
import Sensors
import Interface
import Commands
import threading
import World
import Algorithms
import time
import picamera
import picamera.array
import subprocess
import math
import PositionTracker
from math import pi
import config
import Client

lock = threading.Lock()


class Robot():
    leftMotor = PORT_A
    rightMotor = PORT_D
    rotateMotor = PORT_B
    legoSensorPort = PORT_3
    rightPressurePort = PORT_1
    leftPressurePort = PORT_4
    steeringBias = config.STEERINGBIAS
    mapfile = config.MAP_FILE
    wheelBase = 0.11
    wheelRadius = 0.028
    ticksPerRevolution = 720

    def __init__(self):
        BrickPiSetup()
        self.drive = Commands.Drive(self.leftMotor, self.rightMotor, lock)
        self.sonarLego = Sensors.DistanceSensorLego(self.legoSensorPort, lock)
        self.sonarPCB = Sensors.DistanceSensorPCB()
        self.rotate = Commands.CameraRotate(self.rotateMotor, lock)
        self.world = None
        self.httpClient = None
        if config.USING_API_SERVER:
            self.httpClient = Client.HttpClient(config.API_SERVER_NAME)
            self.world = World.World(self.httpClient.get_world())
        else:
            self.world = World.World(open(self.mapfile).read())
        self.stop = False
        self.lookDirection = 'right'
        self.position = [0.4, 0.4]
        self.position_known = True;
        self.straight = False
        self.lastTicks = None
        self.lastAngle = 0
        self.start_angle = 0
        self.startTicks = self.get_motor_ticks()
        self.lastTicksPos = self.startTicks
        self.path = []
        self.commands = []
        self.follower = None
        self.pathfollower = None
        self.camera = picamera.PiCamera()
        self.camera.resolution = (640, 480)
        if config.STREAMING:
            cmdline = ['avconv', '-v', 'verbose', '-f', 'h264', '-i', '-', '-c:v', 'copy', '-map', '0:0', '-f', 'flv',
                       'rtmp://brons/live/brons']
            player = subprocess.Popen(cmdline, stdin=subprocess.PIPE)
            self.camera.start_recording(player.stdin, format='h264')
        self.moveStartTime = time.time()
        self.lastSpeed = [0, 0]
        self.currentSpeed = [0, 0]
        self.target = []
        self.target_speed = [0, 0]
        self.position_tracker = PositionTracker.PositionTracker(self)

    def move(self, leftSpeed, rightSpeed):
        self.moveStartTime = time.time()
        self.lastSpeed = [self.currentSpeed[0], self.currentSpeed[1]]
        self.currentSpeed = [leftSpeed, rightSpeed]
        self.straight = (leftSpeed == rightSpeed)

    def set_speed(self, leftSpeed, rightSpeed):
        self.target_speed = [leftSpeed, rightSpeed]
        leftBias = 1 + self.steeringBias
        rightBias = 1 - self.steeringBias
        self.drive.move(int(leftSpeed * leftBias), int(rightSpeed * rightBias))

    def update_movement_speed(self):
        timediff = time.time() - self.moveStartTime
        if (time < 0.5):
            speeddiff = [2 * timediff * (self.currentSpeed[0] - self.lastSpeed[0]),
                         2 * timediff * (self.currentSpeed[1] - self.lastSpeed[1])]
            self.set_speed(self.lastSpeed[0] + speeddiff[0], self.lastSpeed[1] + speeddiff[1])
        else:
            self.set_speed(self.currentSpeed[0], self.currentSpeed[1])

    def set_throttle(self, throttle):
        self.drive.setThrottle(throttle)


    def get_forward_distance(self):
        lock.acquire()
        BrickPiUpdateValues()
        lock.release()
        return self.sonarPCB.getValue()


    def get_side_distance(self):
        lock.acquire()
        BrickPiUpdateValues()
        lock.release()
        return self.sonarLego.getValue()


    def get_picture(self):
        with picamera.array.PiRGBArray(self.camera) as stream:
            self.camera.capture(stream, format='bgr', use_video_port=False)
            return stream.array

    def follow(self, direction):
        if not self.follower and not self.pathfollower:
            self.follower = Algorithms.WallFollowerThr(direction, self)
            self.follower.start()
        else:
            Interface.log("Can't start wallfollower, already following")

    def abort(self):
        if self.follower:
            self.follower.stop()
        if self.pathfollower:
            self.pathfollower.stop()
        else:
            Interface.log("Can't abort, not following")

    def reset(self,position,orientation):
        self.position = [position[0] * 0.8 + 0.4, position[1] * 0.8 + 0.4]
        self.reset_orientation()
        self.normalize(self.calculate_orientation(orientation, 0))
        self.position_tracker.clear()

    def get_world(self):
        return self.world


    def get_position(self):
        if self.position_known:
            return self.position
        else:
            return "?"

    def get_path(self, start, stop):
        return self.get_world().find_path(start,stop)

    def rotate_camera(self, direction):
        self.rotate.RotateCamera(direction)
        self.lookDirection = direction

    def nav(self, goal):
        if not self.pathfollower and not self.follower:
            self.pathfollower = Algorithms.ride_with_coordinates(self, goal)
            if not self.pathfollower.recognize_image():
                Interface.log("IMAGE NOT RECOGNIZED :(")
            else:
                self.pathfollower.start()
                # self.pathfollower.running(goal)
                #self.pathfollower = Algorithms.PathFollower(self)
                #self.pathfollower.setGoal(goal)
                #self.pathfollower.start()
        else:
            Interface.log("Can't start navigation, already navigating")

    def nav_coord(self, goal):
        if not self.pathfollower and not self.follower:
            self.pathfollower = Algorithms.ride_with_coordinates(self, goal)
            self.pathfollower.start()
        else:
            Interface.log("Can't start navigation, already navigating")

    def get_motor_ticks(self):
        lock.acquire()
        BrickPiUpdateValues()
        output = [BrickPi.Encoder[Robot.leftMotor], BrickPi.Encoder[Robot.rightMotor]]
        lock.release()
        return output

    #NO USAGE !!!!!!!!!!
    def get_motor_degrees(self):  # 1 tick is 0.5 graden
        ticks = self.get_motor_ticks()
        degrees = [float(ticks[0]) / (Robot.ticksPerRevolution / 360),
                   float(ticks[1]) / (Robot.ticksPerRevolution / 360)]
        return degrees


    def get_motor_distance(self, ticks):  # diameter wiel = 5,6 cm -> r = 2.8
        return [(float(ticks[0]) / Robot.ticksPerRevolution) * 2 * Robot.wheelRadius * pi,
                (float(ticks[1]) / Robot.ticksPerRevolution) * 2 * Robot.wheelRadius * pi]

    def get_orientation(self):
        ticks = self.get_motor_ticks()
        diff = [ticks[0] - self.startTicks[0], ticks[1] - self.startTicks[1]]
        realAngle = float(360 * Robot.wheelRadius * (diff[0] - diff[1])) / float(
            Robot.wheelBase * Robot.ticksPerRevolution)
        return (realAngle + self.start_angle) % 360

    def reset_orientation(self):
        self.startTicks = self.get_motor_ticks()

    def normalize(self, direction):
        normalize_amount = {"N": 270, "E": 0, "S": 90, "W": 180}
        self.start_angle = normalize_amount[direction]

    def rotateDegrees(self, degrees):
        Interface.log("Rotating " + str(degrees) + " Degrees...")
        distance = Robot.wheelBase * pi * degrees / 360
        ticks = (distance / (2 * Robot.wheelRadius * pi)) * Robot.ticksPerRevolution
        preTicks = self.get_motor_ticks()
        direction = 1
        if (degrees < 0):
            direction = -1
        self.move(-direction * 150, direction * 150)
        # Interface.log("start turning until diff > " + str(abs(2 * ticks)))
        while True:
            currentTicks = self.get_motor_ticks()
            diff = [currentTicks[0] - preTicks[0], currentTicks[1] - preTicks[1]]
            # Interface.log("diff = " + str(abs(diff[0] - diff[1])))
            if (abs(diff[0] - diff[1]) > abs(2 * ticks) - 120):
                break
        self.move(0, 0)
        time.sleep(0.5)
        currentTicks = self.get_motor_ticks()
        realDiff = [preTicks[0] - currentTicks[0], preTicks[1] - currentTicks[1]]
        realAngle = float(360 * Robot.wheelRadius * (realDiff[0] - realDiff[1])) / float(
            Robot.wheelBase * Robot.ticksPerRevolution)
        Interface.log("Rotated: " + str(realAngle) + " Degrees")


    def drive_distance(self, distance, direction):
        Interface.log("moving " + str(distance) + " cm")
        ticks = (distance / (2 * Robot.wheelRadius * pi)) * Robot.ticksPerRevolution
        preTicks = self.get_motor_ticks()
        self.move(200, 200)
        Interface.log("start moving until > " + str(ticks))
        while True:
            currentTicks = self.get_motor_ticks()
            if (currentTicks[0] - preTicks[0] > ticks - 180):
                break
            tempDistance = float(2 * Robot.wheelRadius * pi * (currentTicks[0] - preTicks[0])) / float(
                Robot.ticksPerRevolution)
            Interface.log(str(tempDistance))
            self.correct_bias()
            self.move(200, 200)
            time.sleep(0.05)
            self.correct_direction(direction)
            time.sleep(0.05)
        self.move(0, 0)
        time.sleep(0.5)
        realDistance = float(2 * Robot.wheelRadius * pi * (self.get_motor_ticks()[0] - preTicks[0])) / float(
            Robot.ticksPerRevolution)
        Interface.log("real distance: " + str(realDistance))

    def correct_direction(self, direction):
        currentOrientation = self.get_orientation()
        normalized = (currentOrientation - direction + 360) % 360
        if (normalized > 89 and normalized <= 180):
            self.rotateDegrees(normalized)
            return
        elif (normalized > 180 and normalized < 271):
            self.rotateDegrees(normalized - 360)
            return
        errorDirection = (normalized > 0 and normalized <= 180)

        if ((not errorDirection) and (normalized)):
            normalized = 360 - normalized
        Interface.log("Angle Difference: " + str(normalized))

        scale = abs(normalized) * config.NAVIGATION_CORRECTION_SCALE

        if (errorDirection):
            Interface.log("Going Left")
            self.move(config.NAVIGATION_SPEED - scale, config.NAVIGATION_SPEED + scale)
        else:
            Interface.log("Going Right")
            self.move(config.NAVIGATION_SPEED + scale, config.NAVIGATION_SPEED - scale)
        startTime = time.time()
        while True:
            currentOrientation = self.get_orientation()
            normalized = (currentOrientation - direction) % 360
            newError = (normalized > 0 and normalized <= 180)
            if (time.time() - startTime > 1) or (newError != errorDirection):
                break
        self.move(config.NAVIGATION_SPEED, config.NAVIGATION_SPEED)


    def correct_bias(self):
        Interface.log("correcting Bias")
        if not self.straight:
            self.lastTicks = None
            return
        if self.lastTicks == None:
            self.lastTicks = self.get_motor_ticks()
            return
        ticks = self.get_motor_ticks()
        diff = [ticks[0] - self.lastTicks[0], ticks[1] - self.lastTicks[1]]
        if diff[0] > diff[1]:
            self.steeringBias -= 0.01
        elif diff[0] < diff[1]:
            self.steeringBias += 0.01
        self.lastTicks = ticks

    def update_position(self):
        self.position_tracker.execute()

    def calculate_orientation(self, wall, arm_orientation):
        directions = {"N": 270, "E": 0, "S": 90, "W": 180}
        wind_directions = {270: "N", 0: "E", 90: "S", 180: "W"}
        wall_direction = directions[wall]
        return wind_directions[(wall_direction - arm_orientation) % 360]

    def get_block(self, x, y):
        blocksize = 0.8
        block_x = int(x / blocksize)
        block_y = int(y / blocksize)
        return self.get_world().get_block(block_x,block_y)

    def get_wind_direction(self):
        if self.position_known:
            orientation = self.getOrientation()
            if (orientation <= 45 or orientation > 315):
                return "E"
            elif orientation <= 135:
                return "S"
            elif orientation <= 225:
                return "W"
            else:
                return "N"
        else:
            return "?"

    def post_own_position(self):
        fine_position = self.get_position()
        position = self.get_world().position_to_index(self.get_block(fine_position[0],fine_position[1]).get_pos())
        self.httpClient.post_position("brons",position,self.get_wind_direction())

    def calculate_coordinates(self,posnumber):
            col,row = self.get_world().index_to_position(posnumber)
            position = [col * 0.8 + 0.4,
                        row * 0.8 + 0.4]
            return position

robot = Robot()
