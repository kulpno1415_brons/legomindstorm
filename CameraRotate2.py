from BrickPi import *
import threading


class CameraRotate:
    def __init__(self, MotorCamera):
	BrickPiSetup()
        self.MotorCamera = MotorCamera
        BrickPi.MotorEnable[self.MotorCamera] = 1
	self.Direction = 'right'
	self.counter = 0
	self.sensorPort = 0
    def pressureThread(self):
	while (self.counter<=1):
		BrickPiUpdateValues()
		print(BrickPi.Sensor[self.sensorPort])
		if(BrickPi.Sensor[self.sensorPort] == 1):
			self.counter +=1
		if(BrickPi.Sensor[self.sensorPort] == 0):
			self.counter = 0
    def rotationThread(self):
	if (self.Direction == 'left'):
           	power = 30
        if (self.Direction == 'right'):
               	power = -30        
	loopcounter = 0
        while ((power < 256) & (power > -256) & (self.counter<=1) & (loopcounter<40)):
        	
            	if ((self.Direction == 'left') &(power<200)):
                	power = power + 10
            	if ((self.Direction == 'right')& (power>-200)):
                	power = power - 10
		BrickPi.MotorSpeed[self.MotorCamera] = power
		time.sleep(0.5)
		loopcounter = loopcounter + 1
        if (loopcounter == 40):
            return -1	
    def RotateCamera(self, Direction):
	self.Direction = Direction
	if (self.Direction == 'left'):
            self.sensorPort = PORT_1
	if (self.Direction == 'right'):
            self.sensorPort = PORT_4
	BrickPi.SensorType[self.sensorPort] = TYPE_SENSOR_TOUCH
	BrickPiSetupSensors()
	self.t1 = threading.Thread(target = self.pressureThread,args = [])
	self.t2 = threading.Thread(target = self.rotationThread,args = [])
	self.t1.start()
	self.t2.start()
   
		
		