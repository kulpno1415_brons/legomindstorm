import threading
import math
import time
from time import sleep

import Interface
import ImageRecognition
import ShapeRecognition


# ###############################################################

class Algorithm():
    def __init__(self, robot):
        self.set_robot(robot)

    def set_robot(self, robot):
        self.robot = robot

    def get_robot(self):
        return self.robot


# ###############################################################


class translate_path(Algorithm):
    def __init__(self, robot, start, stop):
        Algorithm.__init__(self, robot)
        self.set_path(self.get_robot().get_path(start, stop))

    def get_robot(self):
        return self.robot

    def set_robot(self, robot):
        self.robot = robot

    def get_path(self):
        return self.path

    def set_path(self, path):
        self.path = path

    def get_direction(self, number):
        pos1 = self.get_path()[number].get_pos()
        pos2 = self.get_path()[number + 1].get_pos()
        if pos1[0] == pos2[0]:
            if pos1[1] > pos2[1]:
                return 'N'
            else:
                return 'S'
        elif pos1[1] == pos2[1]:
            if pos1[0] > pos2[0]:
                return 'W'
            else:
                return 'E'
        else:
            return None

    def get_common_wall(self, number):

        walls1 = (self.get_path()[0]).get_walls()
        walls2 = (self.get_path()[1]).get_walls()
        result = []
        for elem in walls1:
            if elem in walls2:
                result.append(elem)
        return result

    # NO USAGE !!!!!
    def get_usefull_wall(self, number):
        direct = self.get_direction(number)
        if direct == None:
            return None
        walls = self.get_common_wall(number)
        if len(walls) == 0:
            return None
        if direct == 'N' or direct == 'S':
            if 'E' in walls:
                return 'E'
            elif 'W' in walls:
                return 'W'
            else:
                return None
        elif direct == 'E' or direct == 'W':
            if 'N' in walls:
                return 'N'
            elif 'S' in walls:
                return 'S'
            else:
                return None

    def translate(self):
        result = []
        for i in range(0, len(self.get_path()) - 1):
            result.append({"type": "turn",
                           "direction": self.get_direction(i),
                           "block": str(self.get_path()[i].get_pos())
            })
            # if self.getUsefullWall(i) == None:
            result.append({"type": "blind", "direction": self.get_direction(i)})
            #else:
            #result.append({"type": "follow", "direction": self.getUsefullWall(i)})
        return result

def turn_direction(robot, current, target):
    quarterTurnTime = 0.9
    halfTurnTime = 1.5
    speed = 150
    neighbors = {
        "N": {"W": 90, "E": -90, "S": 180, "N": 0},
        "E": {"N": 90, "S": -90, "W": 180, "E": 0},
        "S": {"E": 90, "W": -90, "N": 180, "S": 0},
        "W": {"S": 90, "N": -90, "E": 180, "W": 0}
    }
    robot.rotate_degrees(neighbors[current][target])


def move_blind(robot, direction):
    directions = {"N": 0, "E": 90, "S": 180, "W": 270}
    robot.drive_distance(0.80, directions[direction])

# NO USAGE !!!!!
class path_follower(threading.Thread):
    def __init__(self, robot):
        threading.Thread.__init__(self)
        self.follower = None
        self.goal = None
        self.startloc = None
        self.orientation = None
        self.lock = threading.Lock()
        self.running = False
        self.robot = robot

    def set_goal(self, goalloc):
        self.goal = goalloc

    def set_start(self, startLoc):
        self.startloc = startLoc

    def setOrientation(self, orient):
        self.orientation = orient
    def stop(self):
        self.lock.acquire()
        self.running = False
        self.lock.release()
    def isRunning(self):
        self.lock.acquire()
        running_ = self.running
        self.lock.release()
        return running_
    def setRunning(self):
        self.lock.acquire()
        self.running = True
        self.lock.release()

    def run(self):
        self.follower = follow_path(self.robot)
        if type(self.goal) == int and type(self.startloc) == int and len(self.orientation) == 1:
            self.setRunning()
            self.follower.setup_blind(self.startloc, self.goal, self.orientation)
            self.robot.pathfollower = None
        elif type(self.goal) == int:
            self.setRunning()
            self.follower.setup(self.goal)
            self.robot.pathfollower = None
        else:
            Interface.log("Navigation: no goal has been set")


class follow_path(Algorithm):

    def __init__(self, robot):
        Algorithm.__init__(self, robot)
        self.follow_thread = robot.pathfollower

    def setup(self,goal):
        image_recog = ShapeRecognition.ShapeRecognition(self.robot)
        look_direction = "left"
        camera_orientation = -90
        self.robot.rotate_camera(look_direction)
        dist = self.robot.get_side_distance()
        shape = image_recog.getcolouredshape(dist)
        if len(shape) != 2:
            look_direction = "right"
            camera_orientation = 90
            self.robot.rotate_camera(look_direction)
            dist = self.robot.get_side_distance()
            shape = image_recog.getcolouredshape(dist)
            if len(shape) != 2:
                Interface.log("Couldn't recognize shape")
                return
        shape_text = shape[0] + shape[1]
        block = self.robot.get_world().get_block_from_shape(shape_text)
        if not block:
            Interface.log("Couldn't find block with shape " + str(shape_text))
            return
        else:
            Interface.log("Recognized: " + str(shape) + ", facing " + str(block.get_image_orientation()))
        self.currentDirection = self.robot.calculate_orientation(block.get_image_orientation(), camera_orientation)
        self.robot.normalize(self.currentDirection)
        path = translate_path(self.robot, block.get_pos(), goal)
        Interface.log(self.robot.get_world().getPosFromBlocks(path.get_path()))
        Interface.log(self.robot.get_orientation())
        self.robot.path = path.get_path()
        self.robot.commands = path.translate()
        self.go(path.translate())
    def setup_blind(self,start,goal,orientation):
        self.currentDirection = self.robot.calculate_orientation(orientation, 0)
        self.robot.position = [ start.get_pos()[0] * 0.8 + 0.4,
                                start.get_pos()[1] * 0.8 + 0.4]
        self.robot.normalize(self.currentDirection)
        path = translate_path(self.robot, start, goal)
        Interface.log(self.robot.get_world().getPosFromBlocks(path.get_path()))
        Interface.log(self.robot.get_orientation())
        self.robot.path = path.get_path()
        self.robot.commands = path.translate()
        self.go(path.translate())
    def go(self, commands):
        self.robot.reset_orientation()
        for command in commands:
            if(not self.follow_thread.isRunning()):
                Interface.log("Aborting...")
                break
            if (command["type"] == "turn"):
                Interface.log("turning to " + str(command["direction"]) + " from " + str(self.currentDirection))
                turn_direction(self.robot, self.currentDirection, command["direction"])
                self.currentDirection = command["direction"]
            elif (command["type"] == "follow"):
                Interface.log("following " + str(command["direction"]) + " wall")
                # move_blind(self.robot)
            elif (command["type"] == "blind"):
                Interface.log("blindly going forward:" + str(command['direction']))
                move_blind(self.robot, command['direction'])
            Interface.log("Orientation: " + str(self.robot.get_orientation()))


class ride_with_coordinates(Algorithm,threading.Thread):

    def __init__(self, robot,_goal):
        threading.Thread.__init__(self)
        Algorithm.__init__(self, robot)
        self.oldCurrentBlock = None
        self.lock = threading.Lock()
        self.goal = _goal
        self.stopped = False
        self.lastReset = 0

    def run(self):
        self.running(self.goal)

    # NO USAGE !!!!!
    def determine_position(self):
        rotateSpeed = 200
        stopThres = 1.1
        sleepTime = 0.2
        block = self.robot.get_block(self.robot.goals[0][0],self.robot.goals[0][1])
        Interface.log("Number of walls: " + str(len(block.get_walls())))
        if len(block.get_walls()) == 2:
            if "N" in block.get_walls() and "S" in block.get_walls():
                return
            if "E" in block.get_walls() and "W" in block.get_walls():
                return
            if abs(self.robot.goals[0][0] - self.robot.get_position()[0]) > 0.2 or abs(self.robot.goals[0][1] - self.robot.get_position()[1]) > 0.2:
                return
            Interface.log("Determining position, please wait.")
            self.robot.position_tracker.stop()
            self.lastReset = 1
            #time.sleep(1)

            #Determine Direction
            self.robot.move(0, 0)
            startDist = self.get_median_front(20)
            self.robot.move(rotateSpeed, -rotateSpeed)
            time.sleep(sleepTime)
            self.robot.move(0, 0)
            currDist = self.get_median_front(20)
            if currDist < startDist:
                rotDir = 1
            else:
                rotDir = -1

            minDist = currDist
            sideDist = self.get_median_side(20)
            minTicks = self.robot.get_motor_ticks()
            while minDist * stopThres > currDist or sideDist > 100:
                self.robot.move(rotDir * rotateSpeed, - rotDir * rotateSpeed)
                sleep(sleepTime)
                self.robot.move(0, 0)
                currDist = self.get_median_side(20)
                sideDist = self.get_median_side(20)
                if currDist < minDist:
                    minDist = currDist
                    minTicks = self.robot.get_motor_ticks()

            self.robot.move(0, 0)
            time.sleep(sleepTime)
            Interface.log("STRAIGHTENED")

            forward = self.get_median_front(20)/100.0
            side = self.get_median_side(20)/100.0
            Interface.log("FORWARD:" + str(forward))
            Interface.log("SIDEWAYS:" + str(side))
            if "N" in block.get_walls() and "W" in block.get_walls():
                self.robot.reset_orientation()
                self.robot.normalize("W")
                self.robot.startTicks[0] = self.robot.startTicks[0] - (self.robot.get_motor_ticks()[0] - minTicks[0])
                self.robot.startTicks[1] = self.robot.startTicks[1] - (self.robot.get_motor_ticks()[1] - minTicks[1])
                self.robot.position[0] = self.robot.goals[0][0] - 0.4 + forward
                self.robot.position[1] = self.robot.goals[0][1] - 0.4 + side
            elif "N" in block.get_walls() and "E" in block.get_walls():
                self.robot.reset_orientation()
                self.robot.normalize("N")
                self.robot.startTicks[0] = self.robot.startTicks[0] - (self.robot.get_motor_ticks()[0] - minTicks[0])
                self.robot.startTicks[1] = self.robot.startTicks[1] - (self.robot.get_motor_ticks()[1] - minTicks[1])
                self.robot.position[0] = self.robot.goals[0][0] + 0.4 - side
                self.robot.position[1] = self.robot.goals[0][1] - 0.4 + forward
            elif "S" in block.get_walls() and "E" in block.get_walls():
                self.robot.reset_orientation()
                self.robot.normalize("E")
                self.robot.startTicks[0] = self.robot.startTicks[0] - (self.robot.get_motor_ticks()[0] - minTicks[0])
                self.robot.startTicks[1] = self.robot.startTicks[1] - (self.robot.get_motor_ticks()[1] - minTicks[1])
                self.robot.position[0] = self.robot.goals[0][0] + 0.4 - forward
                self.robot.position[1] = self.robot.goals[0][1] + 0.4 - side
            else:
                self.robot.reset_orientation()
                self.robot.normalize("S")
                self.robot.startTicks[0] = self.robot.startTicks[0] - (self.robot.get_motor_ticks()[0] - minTicks[0])
                self.robot.startTicks[1] = self.robot.startTicks[1] - (self.robot.get_motor_ticks()[1] - minTicks[1])
                self.robot.position[0] = self.robot.goals[0][0] - 0.4 + side
                self.robot.position[1] = self.robot.goals[0][1] + 0.4 - forward
            Interface.log("POSITION DETERMINED!")
            self.robot.position_tracker.start()

    def get_median_front(self, number):
        list = []
        for i in range(1, number):
            list.append(self.robot.get_forward_distance())
        list.sort()
        return list[int(number/2)]

    def get_median_side(self, number):
        list = []
        for i in range(1, number):
            list.append(self.robot.get_side_distance())
        list.sort()
        Interface.log("SideValues: " + str(list))
        return list[int(number/2)]

    def recognize_image(self):
        image_recog = ImageRecognition.ImageRecognition(self.robot)
        look_direction = 'left'
        camera_orientation = -90
        self.robot.rotate_camera(look_direction)
        dist = self.robot.get_side_distance()
        shape = image_recog.start(dist)
        if len(shape) != 2:
            look_direction = "right"
            camera_orientation = 90
            self.robot.rotate_camera(look_direction)
            dist = self.robot.get_side_distance()
            shape = image_recog.start(dist)
            if len(shape) != 2:
                Interface.log("Couldn't recognize shape")
                self.robot.pathfollower = None
                return False
        shape_text = shape[0] + ' ' + shape[1]
        block = self.robot.get_world().get_block_from_shape(shape_text)
        if not block:
            Interface.log("Couldn't find block with shape " + str(shape_text))
            self.robot.pathfollower = None
            return False
        else:
            Interface.log("Recognized: " + str(shape) + ", facing " + str(block.get_image_orientation()))
        currentDir = self.robot.calculate_orientation(block.get_image_orientation(), camera_orientation)
        self.robot.normalize(currentDir)
        x = block.get_pos()[0] * 0.8 + 0.4
        y = block.get_pos()[1] * 0.8 + 0.4
        self.robot.position = [x, y]
        return True

    def stop(self):
        self.lock.acquire()
        self.stopped = True
        self.lock.release()
        self.get_robot().pathfollower = None
        Interface.log("Stopping...")

    def has_stopped(self):
        self.lock.acquire()
        _stopped = self.stopped
        self.lock.release()
        return _stopped

    def running(self, goal):
        currentX = self.robot.get_position()[0]
        currentY = self.robot.get_position()[1]
        self.setup(goal)
        while (len(self.robot.goals) > 0 and not self.has_stopped()):
           # if self.lastReset > 10:
           #     self.determinePosition()
            self.execute()
            self.lastReset += 1

        self.robot.goals = []
        self.robot.move(0,0)
        self.robot.pathfollower = None
        self.robot.target = []
        self.stop()
    #
    # def evasiveManouvers(self):
    #     if (self.getRobot().getForwardDistance() < 0.2):
    #         Interface.log("Collision in " + str(self.getRobot().getForwardDistance()))
    #         time.sleep(1)
    #         if (self.getRobot().getForwardDistance() < 0.2):
    #             self.robot.rotateCamera('right')
    #             if (self.robot.getSideDistance() > 0.2):
    #                 self.robot.rotateDegrees(90)
    #                 self.driveShortBit()
    #             else:
    #                 self.robot.rotateCamera('left')
    #                 if (self.robot.getSideDistance() > 0.2):
    #                     self.robot.rotateDegrees(270)
    #                     self.driveShortBit()
    #                 else:
    #                     self.robot.rotateDegrees(90)
    #                     self.robot.rotateCamera('right')
    #                     if (self.robot.getSideDistance() > 0.2):
    #                         self.robot.rotateDegrees(90)
    #                         self.driveShortBit()
    #                     else:
    #                         time.sleep(2)

    def rotate_degrees(self, direction):
        if (direction - self.robot.get_orientation() > 15):
            self.robot.rotate_degrees(direction - self.robot.get_orientation())

    def execute(self):
        #self.determinePosition()
        simple = False
        currentX = self.robot.get_position()[0]
        currentY = self.robot.get_position()[1]
        if len(self.robot.goals) > 1:
            if self.robot.goals[0][0] == self.robot.goals[1][0]:
                distTrav = currentX - self.robot.goals[0][0]
            elif self.robot.goals[0][1] == self.robot.goals[1][1]:
                distTrav = currentY - self.robot.goals[0][1]
            else:
                distTrav = (currentX - self.robot.goals[0][0]) + (currentY - self.robot.goals[0][1])

            if distTrav < 0:
                distTrav = 0

            dist_last = math.sqrt(math.pow(self.robot.goals[0][0] - currentX,2)
                                  + math.pow(self.robot.goals[0][1] - currentY,2))
            dist_next = math.sqrt(math.pow(self.robot.goals[1][0] - currentX,2)
                                  + math.pow(self.robot.goals[1][1] - currentY,2))

            if dist_next < dist_last:
                self.robot.goals.pop(0)
                Interface.log("Dist Last: " + str(dist_last))
                Interface.log("Dist Next: " + str(dist_next))
                Interface.log("Next Goal: (" + str(self.robot.goals[0][0]) + "," + str(self.robot.goals[0][1]) + ")")
                return

            if len(self.robot.goals) > 2 and not simple:
                target = [self.robot.goals[1][0] + distTrav * (self.robot.goals[2][0] - self.robot.goals[1][0]) / 0.8,
                          self.robot.goals[1][1] + distTrav * (self.robot.goals[2][1] - self.robot.goals[1][1]) / 0.8]
            else:
                target = self.robot.goals[1]
        else:
            dist_next = math.sqrt(math.pow(self.robot.goals[0][0] - currentX,2)
                                  + math.pow(self.robot.goals[0][1] - currentY,2))
            if dist_next < 0.2:
                self.stop()
                Interface.log("Reached Final Goal.")
                return
            target = self.robot.goals[0]
        self.robot.target = target
        target_dir = math.atan2(target[1] - currentY, target[0] - currentX) * 180 / math.pi
        Interface.log("Target:      (" + str(target[0]) + "," + str(target[1]) + ")")
        Interface.log("Orientation: " + str(self.robot.get_orientation()))
        Interface.log("Target Dir:  " + str(target_dir))
        self.robot.correct_direction(target_dir)
        Interface.log("-------------")

        # #Hoek bepalen tussen huidige richting en doel
        # currDir = self.robot.getOrientation() - 90
        # currDirRad = currDir * math.pi / 180
        # dist = math.sqrt(math.pow(target[0] - currentX, 2) + math.pow(target[1] - currentY, 2))
        # thetaRad = math.acos((math.cos(currDirRad)*(target[0] - currentX) + math.sin(currDirRad)*(target[1] - currentY))/dist)
        # theta = thetaRad * 180 / math.pi
        # Interface.log("Theta: " + str(theta))
        # scale = -10
        #
        # self.robot.move(100 - scale*thetaRad, 100 + scale*thetaRad)

    def setup(self, goal):
        t0 = time.clock() #time tracking
        currentX = self.robot.get_position()[0]
        currentY = self.robot.get_position()[1]
        currentBlock = self.robot.get_block(currentX,currentY)
        path = translate_path(self.robot, currentBlock.get_pos(), goal)
        self.robot.path = path.get_path()
        print time.clock() - t0, "Path time"
        self.robot.commands = path.translate()
        print time.clock() - t0, "Path Translate time"
        self.robot.goals = []
        for block in self.robot.path:
            self.robot.goals.append([block.get_pos()[0] * 0.8 + 0.4,block.get_pos()[1] * 0.8 + 0.4])

